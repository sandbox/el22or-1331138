// Initialize.
jQuery(document).ready(function() {
  
  $('#edit-search-theme-form-1').each(function() {
    // Get string value from settings.
    var search_focus_string = Drupal.settings.search_focus.string;
    
    // Set value on page load.
    $(this).val(search_focus_string);
    
    // Focus.
    $(this).focus(function() {
      val = $(this).val();
      if (val == search_focus_string) {
        $(this).val('');
      }
    // Blur.
    }).blur(function() {
      val = $(this).val();
      if (val == '') {
        $(this).val(search_focus_string);
      }
    });
  });
  
});
